REPOS=(
    "howl"
    "libsniffle"
    "libhowl"
    "libsnarl"
    "wiggle"
    "docs"
    "wiggle-app"
    "fifo.js"
    "chunter"
    "jingles"
    "snarl"
    "pyfi"
    "jingles2"
    "sniffle"
    "watchdog"
    "fifo-tools"
    "fifo_utils"
    "fifo_dt"
    "leofs"
    "fifo-db"
    "fifo_s3"
    "libsnarlmatch"
    "fqc"
    "dragon"
    "libchunter"
    "mi-leofs"
    "fifo_spec"
    "fifo_api"
    "ensq_rpc"
    "ensq"
    "libleofs"
    "fifo_test"
    "howl_test"
    "snarl_test"
)

for repo in "${REPOS[@]}"
    do
        git clone git@github.com:project-fifo/$repo.git
        rm -rf $repo/.git
done

echo "DONE"
